function Derive(derivatives){

  this.derivatives = derivatives
  this.inputs = this.listInputs(derivatives)

}

Derive.prototype = {
   constructor: Derive,
   listInputs: function(derivatives){
      console.log("inputs")
      var inputs = []
      derivatives.forEach(function(derivative){
         if(derivative.hasOwnProperty("input") && derivative.input === true){
            inputs.push(derivative.name)
         }
      })
      return inputs
   },
   getValues: function(currentInputs){
      var values = new Map()

      //Check that all inputs are present
      var missingInputs = []
      this.inputs.forEach(function(inputName){
         if(!currentInputs.has(inputName)){
            missingInputs.push(inputName);
         }
      })
      //Send an error in case there are missingInputs
      if (missingInputs.length > 0){
         console.log(missingInputs)
         return {
            error:"missingInputs",
            missingInputs : missingInputs
         }
      }

      //Load currentInputs into the map
      currentInputs.forEach(function(value, key){
         values.set(key, value)
      })

      //If this values hasn't been calculated, run it's calculator and store it's value in the values map
      function GetVariable(name){
         var value
         if(values.has(name)){
            value = values.get(name)
         }else{
           //console.log(name)
            var derivative = derivatives.find(function(element){return element.name === name;})
            if(typeof derivative === 'undefined'){
               console.log({
                  error:"A calculator references an undefined derivative",
                  name:name
               })
               return
            }

            if(!derivative.hasOwnProperty("calculator")){
               console.log({
                  error:"A derivative has a missing calculator",
                  name:name
               })
               return
            }
            value = derivative.calculator(GetVariable)
            values.set(name, value)
         }
         return value
      }
      //Iterate over all the values
      derivatives.forEach(function(derivative){
        GetVariable(derivative.name)
      })

      function SplitName(name){
         var parts = name.split(".")
         var groupName = {}
         if(parts.length > 1){
            groupName.group = parts[0]
            groupName.name = parts[1]
         }else{
            groupName.name = parts[0]
            groupName.group = null
         }
         return groupName
      }
      function IsString(value){
         return typeof value === 'string' || value instanceof String
      }

      //Split groupnames into objects
      var separatedValues = new Map()
      values.forEach(function(value, key){
         var groupedName = SplitName(key)
         //console.log(groupedName)
         if(groupedName.group){
            //Is a grouped name

            //Create the group if it is needed
            if(!separatedValues.has(groupedName.group)){
               separatedValues.set(groupedName.group, new Map())
            }

            //Check that the name isn't used for a individual name already
            if (IsString(separatedValues.get(groupedName.group))){
               return {
                  error:"There is a group with the same name as an individual derivative",
                  value:groupedName.group
               }
            }
            //console.log(separatedValues)

            //Add the value to the group
            separatedValues.get(groupedName.group).set(groupedName.name, value)

         }else{
            //Is an individual name

            //Check that the name isn't used by a group already
            if(separatedValues.has(key)){
               return {
                  error:"There is a group with the same name as an individual derivative",
                  value: key
               }
            }
            separatedValues.set(key, value)
         }
      })

      return separatedValues
   }
}

// Array of objects
// ----------------
// Value object
// name:string
// input:bool
// unit:string
// calculator:function(d), d("name") asks for other values

//At output the names will be divided into objects like this as groupName.valueName
//NB! will throw an error if there is an object with the same name as a group
//Groups can go only one level deep

var derivatives = [
   {
      name:"buildingArea",
      input:true,
      unit:"sqm",
   },
   {
      name:"grantPercentage",
      input:true,
      unit:"%"
   },
   {
      name:"selfFinancePercentage",
      input:true,
      unit:"%",
   },
   {
      name:"costTotal",
      input:true,
      unit:"€"
   },
   ////////////////
   //    SUMS    //
   ////////////////
   {
      name: "sum.kredexGrant",
      unit:"€",
      calculator: function(d){
        return d("costTotal") * d("grantPercentage")
      }
   },
   {
      name: "sum.selfFinance",
      unit:"€",
      calculator: function(d){
         return (d("costTotal")-d("sum.kredexGrant")) * d("selfFinancePercentage")
      }
   },
   {
      name: "sum.loan",
      unit:"€",
      calculator: function(d){
        return d("costTotal") -d("sum.selfFinance") - d("sum.kredexGrant")
      }
   },
   ////////////////
   // BOUNDARIES //
   ////////////////
   {
      name: "boundaries.wallArea",
      unit:"m2",
      calculator: function(d){
        return 1454.12
      }
   },
   {
      name: "boundaries.wallUValue",
      unit:"W/m2*K",
      input:true
   },
   {
      name: "boundaries.roofArea",
      unit:"m2",
      calculator: function(d){
        return 641.6
      }
   },
   {
      name: "boundaries.roofUValue",
      unit:"W/m2*K",
      input:true
   },
   {
      name: "boundaries.cellarArea",
      unit:"m2",
      calculator: function(d){
        return 641.6
      }
   },
   {
      name: "boundaries.cellarUValue",
      unit:"W/m2*K",
      input:true
   },
   {
      name: "boundaries.windowArea",
      unit:"m2",
      calculator: function(d){
        return 490.68
      }
   },
   {
      name: "boundaries.windowUValue",
      unit:"W/m2*K",
      input:true
   },
   {
      name: "boundaries.areaTotal",
      unit:"m2",
      calculator: function(d){
        return d("boundaries.wallArea")+d("boundaries.roofArea")+d("boundaries.cellarArea")+d("boundaries.windowArea")
      }
   },
   {
      name: "boundaries.heatLoss",
      unit:"W/K",
      calculator: function(d){
        return d("boundaries.wallArea")*d("boundaries.wallUValue")
              +d("boundaries.roofArea")*d("boundaries.roofUValue")
              +d("boundaries.cellarArea")*d("boundaries.cellarUValue")
              +d("boundaries.windowArea")*d("boundaries.windowUValue")
      }
   },
   ////////////////
   // T-BRIDGES  //
   ////////////////
   {
      name: "tBridge.cornerLength",
      unit:"m",
      calculator: function(d){
        return 57.2
      }
   },
   {
      name: "tBridge.cornerPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.intWallLength",
      unit:"m",
      calculator: function(d){
        return 550
      }
   },
   {
      name: "tBridge.intWallPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.floorLength",
      unit:"m",
      calculator: function(d){
        return 408
      }
   },
   {
      name: "tBridge.floorPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.balconyLength",
      unit:"m",
      calculator: function(d){
        return 154.1
      }
   },
   {
      name: "tBridge.balconyPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.roofLength",
      unit:"m",
      calculator: function(d){
        return 136
      }
   },
   {
      name: "tBridge.roofPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.cellarLength",
      unit:"m",
      calculator: function(d){
        return 136
      }
   },
   {
      name: "tBridge.cellarPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.windowLength",
      unit:"m",
      calculator: function(d){
        return 1517.17
      }
   },
   {
      name: "tBridge.windowPsi",
      unit:"W/m*K",
      input:true
   },
   {
      name: "tBridge.heatLoss",
      unit:"W/K",
      calculator: function(d){
        return d("tBridge.cornerLength")*d("tBridge.cornerPsi")
              +d("tBridge.intWallLength")*d("tBridge.intWallPsi")
              +d("tBridge.floorLength")*d("tBridge.floorPsi")
              +d("tBridge.roofLength")*d("tBridge.roofPsi")
              +d("tBridge.cellarLength")*d("tBridge.cellarPsi")
              +d("tBridge.balconyLength")*d("tBridge.balconyPsi")
              +d("tBridge.windowLength")*d("tBridge.windowPsi")
      }
   },
   ////////////////
   //VENTILATION //
   ////////////////
   {
      name: "ventilation.flowRatePerSqm",
      unit:"l/s*m2",
      input:true
      // calculator: function(d){
      //   return 0.5
      // }
   },
   {
      name: "ventilation.exchangeEfficiency",
      unit:"1/1",
      input:true
   },
   {
      name: "ventilation.sfp",
      unit:"W/(l/s)",
      input:true
   },
   {
      name: "ventilation.heatingPeriod",
      unit:"h",
      calculator: function(d){
        return 217*24 //The length of the heating period is 217 days
      }
   },
   {
      name: "ventilation.operationPeriod",
      unit:"h",
      calculator: function(d){
        return 365*24
      }
   },
   {
      name: "ventilation.heatingTemperature",
      unit:"h",
      calculator: function(d){
        return 21-(-1.2)
      }
   },
   {
      name: "ventilation.ventEnergy",
      unit:"kWh/m2",
      calculator: function(d){
        return d("ventilation.flowRatePerSqm")*d("ventilation.sfp")*d("ventilation.operationPeriod")/1000
      }
   },
   {
      name: "ventilation.heatingEnergy",
      unit:"kWh/m2",
      calculator: function(d){
        return (d("ventilation.flowRatePerSqm")/1000*1.2*1005)/1000*
               (1-d("ventilation.exchangeEfficiency"))*
               d("ventilation.heatingPeriod")*d("ventilation.heatingTemperature")
      }
   },

   ////////////////
   //INFILTRATION//
   ////////////////
   {
      name: "infiltration.q50",
      unit:"m3/(h*m2)",
      calculator: function(d){
        return 4.7
      }
   },
   {
      name: "infiltration.storyCoefficient",
      unit:"1/1",
      calculator: function(d){
        return 15
      }
   },
   {
      name: "infiltration.yearlyFlow",
      unit:"m3/s",
      calculator: function(d){
        return d("infiltration.q50")*d("boundaries.areaTotal")/(3600*d("infiltration.storyCoefficient"))
      }
   },
   {
      name: "infiltration.heatLoss",
      unit:"W/K",
      calculator: function(d){
        return d("infiltration.yearlyFlow")*1005*1.2
      }
   },
   {
      name: "infiltration.heatingEnergy",
      unit:"kWh/m2*a",
      calculator: function(d){
        return d("infiltration.heatLoss")*d("ventilation.heatingPeriod")*d("ventilation.heatingTemperature")
               /(d("buildingArea")*1000)
      }
   },
   ////////////////
   //NET HEATING //
   ////////////////
   {
      name: "heatingCalc.heatLossTotal",
      unit:"W/K",
      calculator: function(d){
        return d("boundaries.heatLoss")+d("tBridge.heatLoss")+d("infiltration.heatLoss")
      }
   },
   {
      name: "heatingCalc.heatLossTotalPerSqm",
      unit:"W/K*m2",
      calculator: function(d){
        return d("heatingCalc.heatLossTotal")/d("buildingArea")
      }
   },
   {
      name: "heatingCalc.heatingEnergy",
      unit:"kWh/m2*a",
      calculator: function(d){
        return (d("heatingCalc.heatLossTotalPerSqm")*85.6)-13.81
      }
   },

   ////////////////
   //   HEATING  //
   ////////////////
   {
      name: "heating.hotWater",
      unit:"kWh/m2*a",
      calculator: function(d){
        return 30
      }
   },
   {
      name: "heating.roomHeating",
      unit:"kWh/m2*a",
      calculator: function(d){
        return d("heatingCalc.heatingEnergy")
      }
   },
   {
      name: "heating.ventilation",
      unit:"kWh/m2*a",
      calculator: function(d){
        return d("ventilation.heatingEnergy")
      }
   },
   {
      name: "heating.infiltration",
      unit:"kWh/m2*a",
      calculator: function(d){
        return d("infiltration.heatingEnergy")
      }
   },
   {
      name: "heating.distriEff",
      unit:"1/1",
      calculator: function(d){
        return 0.97
      }
   },
   {
      name: "heating.sourceEff",
      unit:"1/1",
      calculator: function(d){
        return 1
      }
   },
   {
      name: "heating.ETAFactor",
      unit:"1/1",
      calculator: function(d){
        return 0.9
      }
   },
   {
      name: "heating.totalHeatingEnergy",
      unit:"kWh(m2*a)",
      calculator: function(d){
        return (d("heating.roomHeating")+d("heating.ventilation")+d("infiltration.heatingEnergy"))/(d("heating.distriEff")*d("heating.sourceEff"))
      }
   },
   {
      name: "heating.ETAEnergy",
      unit:"kWh/m2",
      calculator: function(d){
        return (d("heating.hotWater")+d("heating.roomHeating")+d("heating.ventilation")+d("infiltration.heatingEnergy"))
               /(d("heating.distriEff")*d("heating.sourceEff"))
      }
   },
   ////////////////
   // ELECTRICITY//
   ////////////////
   {
      name: "electricity.heatingPump",
      unit:"kWh/m2",
      calculator: function(d){
        return 0.5
      }
   },
   {
      name: "electricity.lighting",
      unit:"kWh/m2",
      calculator: function(d){
        return 0.1*8*8760/1000
      }
   },
   {
      name: "electricity.equipment",
      unit:"kWh/m2",
      calculator: function(d){
        return 0.6*3*8760/1000/0.7
      }
   },
   {
      name: "electricity.ventilation",
      unit:"kWh/m2",
      calculator: function(d){
        return d("ventilation.ventEnergy")
      }
   },
   {
      name: "electricity.ETAFactor",
      unit:"kWh/m2",
      calculator: function(d){
        return 2
      }
   },
   {
      name: "electricity.commonEnergy",
      unit:"kWh/m2",
      calculator: function(d){
        return d("electricity.heatingPump")+d("electricity.ventilation")
      }
   },
   {
      name: "electricity.ETAEnergy",
      unit:"kWh/m2",
      calculator: function(d){
        return (d("electricity.heatingPump")+d("electricity.ventilation")+d("electricity.lighting")+d("electricity.equipment"))*d("electricity.ETAFactor")
      }
   },
   ////////////////
   //SOLAR PANELS//
   ////////////////
   {
      name: "solar.panelsPower",
      unit:"kWp",
      input:true
   },
   {
      name: "solar.orientationH",
      unit:"1/1",
      calculator: function(d){
        return 1
      }
   },
   {
      name: "solar.orientationV",
      unit:"1/1",
      calculator: function(d){
        return 1.2
      }
   },
   {
      name: "solar.efficiency",
      unit:"1/1",
      calculator: function(d){
        return 0.75
      }
   },
   {
      name: "solar.yearlyRadiation",
      unit:"kWh/m2*a",
      calculator: function(d){
        return 1152
      }
   },
   {
      name: "solar.standardRadiation",
      unit:"kW/m2",
      calculator: function(d){
        return 1
      }
   },
   {
      name: "solar.productionYearly",
      unit:"kWh/a",
      calculator: function(d){
        return -d("solar.yearlyRadiation")*d("solar.panelsPower")
               *d("solar.efficiency")*d("solar.orientationV")*d("solar.orientationH")
               /d("solar.standardRadiation")
      }
   },
   {
      name: "solar.productionYearlyPerSqm",
      unit:"kWh/a*m2",
      calculator: function(d){
        return d("solar.productionYearly")/d("buildingArea")
      }
   },
   {
      name: "solar.ETAProduction",
      unit:"kWh/a*m2",
      calculator: function(d){
        return d("solar.productionYearlyPerSqm")*d("electricity.ETAFactor")
      }
   },
   ////////////////
   //     ETA    //
   ////////////////
   {
      name: "eta.heating",
      unit:"kWh/a*m2",
      calculator: function(d){
        return d("heating.ETAEnergy")
      }
   },
   {
      name: "eta.solar",
      unit:"kWh/a*m2",
      calculator: function(d){
        return d("solar.ETAProduction")
      }
   },
   {
      name: "eta.electricity",
      unit:"kWh/a*m2",
      calculator: function(d){
        return d("electricity.ETAEnergy")
      }
   },
   {
      name: "eta.total",
      unit:"kWh/a*m2",
      calculator: function(d){
        return d("eta.heating")+d("eta.solar")+d("eta.electricity")
      }
   },
   ////////////////
   //   PRICES   //
   ////////////////
   {
      name: "price.electricity",
      unit:"€/kWh",
      calculator: function(d){
        return 0.11
      }
   },
   {
      name: "price.heating",
      unit:"€/kWh",
      calculator: function(d){
        return 0.08
      }
   },
   ////////////////
   // UTILITIES  //
   ////////////////
   {
      name: "utilities.heating",
      unit:"€/m2",
      calculator: function(d){
        return d("heating.totalHeatingEnergy")*d("price.heating")
      }
   },
   {
      name: "utilities.electric",
      unit:"€/m2",
      calculator: function(d){
        return d("electricity.commonEnergy")*d("price.electricity")
      }
   },
   {
      name: "utilities.loanPayment",
      unit:"€/m2",
      calculator: function(d){
        return d("loan.yearlySum")/d("buildingArea")
      }
   },
   {
      name: "utilities.energyProduction",
      unit:"€/m2",
      calculator: function(d){
        return d("solar.productionYearlyPerSqm")*d("price.electricity")
      }
   },
   {
      name: "totalUtilities",
      unit:"€/m2",
      calculator: function(d){
        return d("utilities.heating")+d("utilities.electric")+d("utilities.loanPayment")+d("utilities.energyProduction")
      }
   },
   ////////////////
   //    LOAN    //
   ////////////////
   {
      name: "loan.durationYears",
      unit:"year",
      calculator: function(d){
        return 20
      }
   },
   {
      name: "loan.interestYear",
      unit:"%",
      calculator: function(d){
        return 0.05
      }
   },
   {
      name: "loan.durationMonths",
      unit:"month",
      calculator: function(d){
        return d("loan.durationYears")*12
      }
   },
   {
      name: "loan.interestMonth",
      unit:"/",
      calculator: function(d){
        return d("loan.interestYear")/12
      }
   },
   {
      name: "loan.annuityMonth",
      unit:"€",
      calculator: function(d){
        return d("sum.loan")*d("loan.interestMonth")* (1-(Math.pow(1+d("loan.interestMonth"),-d("loan.durationMonths"))))
      }
   },
   {
      name: "loan.yearlySum",
      unit:"€",
      calculator: function(d){
        return d("loan.annuityMonth")*12
      }
   }
]

var inputs = new Map([
   ["buildingArea",           2960],
   ["grantPercentage",        0.4],
   ["selfFinancePercentage",  0.15],
   ["sum.total",              50000],

   ["boundaries.wallUValue",   1],
   ["boundaries.roofUValue",   1],
   ["boundaries.cellarUValue", 1],
   ["boundaries.windowUValue", 2],

   ["tBridge.cornerPsi",    1],
   ["tBridge.intWallPsi",   0],
   ["tBridge.floorPsi",     0],
   ["tBridge.balconyPsi",   0.5],
   ["tBridge.roofPsi",      0.4],
   ["tBridge.cellarPsi",    0.2],
   ["tBridge.windowPsi",    0.3],

   ["ventilation.exchangeEfficiency",   0],
   ["ventilation.sfp",                  0.4],
   ["ventilation.flowRatePerSqm",       0.5],
   ["solar.panelsPower",                10]
])


var deriver = new Derive(derivatives)
//console.log(deriver.getValues(inputs))
//var vals = []

// var currentInputs = [
//    {
//       name:"sum-Total",
//       value: 300000
//    },
//    {
//       name:"grantPercentage",
//       value: 0.4
//    },
//    {
//       name:"solarPanels-Power",
//       value: 30
//    },
//    {
//       name:"heatingReductionFactor",
//       value: 0.5
//    },
//    {
//       name:"addedElectricityConsumption",
//       value: 20
//    }
// ]

// MyDerivatives.getValues(currentInputs).forEach(function(v,k){
//    vals.push({key:k, value:v})
//   //console.log(k+": "+v)
// })
// vals.sort(function(a,b){
//    if(a.key < b.key) return -1;
//    if(a.key > b.key) return 1;
//    return 0;
// })
// vals.forEach(function(d){
//    console.log(d.key+": "+d.value)
// })
