INPUT_CHOICE = [
  {
    "id": "nullvalik",
    "optionId": "so-fas",
    "optionName": "Fassaadi soojustamine",
    "tabName": "Soojustamine",
    "name": "Ei soojustata",
    "price": 0,
    "description": "",
    "grantType": 0,
    "backgroundImage": "img/soojustamine-sein-vana.png",
    "boundaries.wallUValue":1,
    "tBridge.cornerPsi":0.7,
    "tBridge.balconyPsi":0.2,
    "tBridge.intWallPsi":0.3,
    "tBridge.floorPsi":0.5
  },
  {
    "id": "150mm",
    "optionId": "so-fas",
    "optionName": "Fassaadi soojustamine",
    "tabName": "Soojustamine",
    "name": "150mm soojustus",
    "price": 85800,
    "description": "",
    "buttonImage": "",
    "grantType": 25,
    "backgroundImage": "img/soojustamine-sein-uus.png",
    "boundaries.wallUValue":0.2,
    "tBridge.cornerPsi":0.15,
    "tBridge.balconyPsi":0.3,
    "tBridge.intWallPsi":0.01,
    "tBridge.floorPsi":0.01
  },
  {
    "id": "250mm",
    "optionId": "so-fas",
    "optionName": "Fassaadi soojustamine",
    "tabName": "Soojustamine",
    "name": "250mm soojustus",
    "price": 92400,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-sein-uus.png",
    "boundaries.wallUValue":0.15,
    "tBridge.cornerPsi":0.15,
    "tBridge.balconyPsi":0.3,
    "tBridge.intWallPsi":0.01,
    "tBridge.floorPsi":0.01
  },
  {
    "id": "300mm",
    "optionId": "so-fas",
    "optionName": "Fassaadi soojustamine",
    "tabName": "Soojustamine",
    "name": "300mm soojustus",
    "price": 105600,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-sein-uus.png",
    "boundaries.wallUValue":0.12,
    "tBridge.cornerPsi":0.15,
    "tBridge.balconyPsi":0.3,
    "tBridge.intWallPsi":0.01,
    "tBridge.floorPsi":0.01
  },
  {
    "id": "nullvalik",
    "optionId": "so-akn",
    "optionName": "Akende soojustamine/vahetamine",
    "tabName": "Soojustamine",
    "name": "Ei vahetata",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 0,
    "backgroundImage": "img/soojustamine-aken-vana.png",
    "boundaries.windowUValue":1.6,
    "tBridge.windowPsi":0.5
  },
  {
    "id": "pale",
    "optionId": "so-akn",
    "optionName": "Akende soojustamine/vahetamine",
    "tabName": "Soojustamine",
    "name": "Soojustatakse aknapaled",
    "price": 22800,
    "description": "",
    "buttonImage": "",
    "grantType": 25,
    "backgroundImage": "img/soojustamine-aken-uus.png",
    "boundaries.windowUValue":1.6,
    "tBridge.windowPsi":0.35
  },
  {
    "id": "pinda",
    "optionId": "so-akn",
    "optionName": "Akende soojustamine/vahetamine",
    "tabName": "Soojustamine",
    "name": "Tõstetakse soojustuse tasapinda",
    "price": 201800,
    "description": "Eeldab kõikide akende vahetamist",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-aken-uus.png",
    "boundaries.windowUValue":0.8,
    "tBridge.windowPsi":0.02
  },
  {
    "id": "nullvalik",
    "optionId": "so-sos",
    "optionName": "Sokli soojustamine",
    "tabName": "Soojustamine",
    "name": "Ei soojustata",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 0,
    "backgroundImage": "img/soojustamine-sokkel-vana.png",
    "boundaries.cellarUValue":1,
    "tBridge.cellarPsi":0.4
  },
  {
    "id": "keld",
    "optionId": "so-sos",
    "optionName": "Sokli soojustamine",
    "tabName": "Soojustamine",
    "name": "Keldri lagi 100mm",
    "price": 19200,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-sokkel-uus.png",
    "boundaries.cellarUValue":0.25,
    "tBridge.cellarPsi":0.4
  },
  {
    "id": "sok",
    "optionId": "so-sos",
    "optionName": "Sokli soojustamine",
    "tabName": "Soojustamine",
    "name": "Sokkel 100mm",
    "price": 10900,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-sokkel-uus.png",
    "boundaries.cellarUValue":0.20,
    "tBridge.cellarPsi":0.3
  },
  {
    "id": "keld+sok",
    "optionId": "so-sos",
    "optionName": "Sokli soojustamine",
    "tabName": "Soojustamine",
    "name": "Keldri lagi 100mm + sokkel 100mm",
    "price": 30100,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-sokkel-uus.png",
    "boundaries.cellarUValue":0.15,
    "tBridge.cellarPsi":0.05
  },
  {
    "id": "nullvalik",
    "optionId": "so-kas",
    "optionName": "Katuse soojustamine",
    "tabName": "Soojustamine",
    "name": "Ei soojustata",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 0,
    "backgroundImage": "img/soojustamine-katus-vana.png",
    "boundaries.roofUValue":0.7,
    "tBridge.roofPsi":0.55
  },
  {
    "id": "varem",
    "optionId": "so-kas",
    "optionName": "Katuse soojustamine",
    "tabName": "Soojustamine",
    "name": "Varem teostatud",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-katus-uus.png",
    "boundaries.roofUValue":0.25,
    "tBridge.roofPsi":0.3
  },
  {
    "id": "teht",
    "optionId": "so-kas",
    "optionName": "Katuse soojustamine",
    "tabName": "Soojustamine",
    "name": "300mm soojustus koos katuse vahetamisega",
    "price": 92400,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/soojustamine-katus-uus.png",
    "boundaries.roofUValue":0.12,
    "tBridge.roofPsi":0.3
  },
  {
    "id": "nullvalik",
    "optionId": "en-ppa",
    "optionName": "Päikesepaneelid",
    "tabName": "Lokaalne energiatootmine",
    "name": "Ei paigaldata",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 40
  },
  {
    "id": "10kw",
    "optionId": "en-ppa",
    "optionName": "Päikesepaneelid",
    "tabName": "Lokaalne energiatootmine",
    "name": "10 kWp päikesejaam",
    "price": 15000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "img/tehno-päike-1.png",
    "grantType": 40,
    "solarPower" : 10
  },
  {
    "id": "20kw",
    "optionId": "en-ppa",
    "optionName": "Päikesepaneelid",
    "tabName": "Lokaalne energiatootmine",
    "name": "20 kWp päikesejaam",
    "price": 28000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "img/tehno-päike-2.png",
    "grantType": 40,
    "solarPower" : 20
  },
  {
    "id": "30kw",
    "optionId": "en-ppa",
    "optionName": "Päikesepaneelid",
    "tabName": "Lokaalne energiatootmine",
    "name": "30 kWp päikesejaam",
    "price": 39000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "img/tehno-päike-3.png",
    "grantType": 40,
    "solarPower" : 30
  },
  {
    "id": "nullvalik",
    "optionId": "te-ven",
    "optionName": "Ventilatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Ei renoveerita",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 0,
    "backgroundImage": "img/tehno-vent-nat.png",
    "ventilation.flowRatePerSqm":0,
    "ventilation.exchangeEfficiency":0,
    "ventilation.sfp":0
  },
  {
    "id": "meh",
    "optionId": "te-ven",
    "optionName": "Ventilatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Mehhaaniline väljatõmme",
    "price": 48000,
    "description": "",
    "buttonImage": "",
    "grantType": 25,
    "backgroundImage": "img/tehno-vent-meh.png",
    "ventilation.flowRatePerSqm":0.5,
    "ventilation.exchangeEfficiency":0,
    "ventilation.sfp":0.4
  },
  {
    "id": "sis",
    "optionId": "te-ven",
    "optionName": "Ventilatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Soojatagastusega väljatõmme",
    "price": 180000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/tehno-vent-välja.png",
    "ventilation.flowRatePerSqm":0.5,
    "ventilation.exchangeEfficiency":0.75,
    "ventilation.sfp":1.8
  },
  {
    "id": "val",
    "optionId": "te-ven",
    "optionName": "Ventilatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Soojatagastusega sissepuhe",
    "price": 210000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/tehno-vent-sisse.png",
    "ventilation.flowRatePerSqm":0.5,
    "ventilation.exchangeEfficiency":0.7,
    "ventilation.sfp":1.8
  },
  {
    "id": "ind",
    "optionId": "te-ven",
    "optionName": "Ventilatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Individuaalne korteripõhine ventilatsiooniagregaat",
    "price": 300000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/tehno-vent-indiv.png",
    "ventilation.flowRatePerSqm":0.5,
    "ventilation.exchangeEfficiency":0.8,
    "ventilation.sfp":1.8
  },
  {
    "id": "nullvalik",
    "optionId": "te-kys",
    "optionName": "Küttesüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Ei renoveerita",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 0,
    "backgroundImage": "img/tehno-kyte-algne.png"
  },
  {
    "id": "rek",
    "optionId": "te-kys",
    "optionName": "Küttesüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Soojasõlme rekonstrueerimine",
    "price": 6000,
    "description": "Soojustamine eeldab soojasõlme kohandamist",
    "buttonImage": "",
    "grantType": 0,
    "backgroundImage": "img/tehno-kyte-katel.png"
  },
  {
    "id": "teht",
    "optionId": "te-kys",
    "optionName": "Küttesüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Kahetorusüsteemi väljaehitamine",
    "price": 90000,
    "description": "Vajalik radiaatorite korteripõhiseks reguleerimiseks",
    "buttonImage": "",
    "grantType": 25,
    "backgroundImage":"img/tehno-kyte-kahetoru.png"
  },
  {
    "id": "nullvalik",
    "optionId": "te-vek",
    "optionName": "Vee- ja kanalisatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Ei renoveerita",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 40
  },
  {
    "id": "teht",
    "optionId": "te-vek",
    "optionName": "Vee- ja kanalisatsioonisüsteem",
    "tabName": "Tehnosüsteemid",
    "name": "Kaasajastatakse",
    "price": 18000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 40
  },
  {
    "id": "nullvalik",
    "optionId": "tr-tam",
    "optionName": "Trepikoja eeskoda",
    "tabName": "Trepikojad",
    "name": "Ei renoveerita",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "backgroundImage":"img/trepp-01.png",
    "grantType": 40
  },
  {
    "id": "yhend",
    "optionId": "tr-tam",
    "optionName": "Trepikoja eeskoda",
    "tabName": "Trepikojad",
    "name": "Eeskojad ühendatakse",
    "price": 8000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage":"img/trepp-02.png"
  },
  {
    "id": "valja",
    "optionId": "tr-tam",
    "optionName": "Trepikoja eeskoda",
    "tabName": "Trepikojad",
    "name": "Ehitatakse väljaulatuv eeskoda",
    "price": 15000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage":"img/trepp-03.png"
  },
  {
    "id": "nullvalik",
    "optionId": "tr-int",
    "optionName": "Trepikoja interjöör",
    "tabName": "Trepikojad",
    "name": "Ei renoveerita",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 40
  },
  {
    "id": "teht",
    "optionId": "tr-int",
    "optionName": "Trepikoja interjöör",
    "tabName": "Trepikojad",
    "name": "Seinte viimistlemine",
    "price": 3000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 40
  },
  {
    "id": "nullvalik",
    "optionId": "fa-rod",
    "optionName": "Rõdud",
    "tabName": "Fasaadid",
    "name": "Ei renoveerita",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/fassaad-r-0.png"
  },
  {
    "id": "piire",
    "optionId": "fa-rod",
    "optionName": "Rõdude renoveerimine",
    "tabName": "Fasaadid",
    "name": "Piirete renoveerimine",
    "price": 96000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/fassaad-r-1.png"
  },
  {
    "id": "klaas",
    "optionId": "fa-rod",
    "optionName": "Rõdude renoveerimine",
    "tabName": "Fasaadid",
    "name": "Piirete renoveerimine ja klaasimine",
    "price": 144000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/fassaad-r-2.png"
  },
  {
    "id": "uus",
    "optionId": "fa-rod",
    "optionName": "Rõdude renoveerimine",
    "tabName": "Fasaadid",
    "name": "Rõdude asendamine uutega",
    "price": 240000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/fassaad-r-2.png"
  },
  {
    "id": "nullvalik",
    "optionId": "fa-ter",
    "optionName": "Terrasside ehitamine 1. korrusele",
    "tabName": "Fasaadid",
    "name": "Ei",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "",
    "grantType": 40
  },
  {
    "id": "teht",
    "optionId": "fa-ter",
    "optionName": "Terrasside ehitamine 1. korrusele",
    "tabName": "Fasaadid",
    "name": "Jah",
    "price": 3000,
    "description": "",
    "buttonImage": "",
    "grantType": 40,
    "backgroundImage": "img/fassaad-t-1.png"
  },
  {
    "id": "krohv",
    "optionId": "fa-fas",
    "optionName": "Fassaadi kujundus",
    "tabName": "Fasaadid",
    "name": "Krohvitud",
    "price": 0,
    "description": "",
    "buttonImage": "",
    "grantType": 40
  },
  {
    "id": "superg",
    "optionId": "fa-fas",
    "optionName": "Fassaadi kujundus",
    "tabName": "Fasaadid",
    "name": "Krohvitud, Sprayprinteri supergraafikaga",
    "price": 5000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "img/fassaad-f-1.png",
    "grantType": 40,
    "moreInfo": true
  },
  {
    "id": "kombo",
    "optionId": "fa-fas",
    "optionName": "Fassaadi kujundus",
    "tabName": "Fasaadid",
    "name": "Kombineeritud puit-krohv",
    "price": 35000,
    "description": "",
    "buttonImage": "",
    "backgroundImage": "img/fassaad-f-2.png",
    "grantType": 40
  }
]
